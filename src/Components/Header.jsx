import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import './Header.css'

function Header() {
  return (
    <Navbar bg="light" expand="lg">
      <Container fluid>
      <img src="https://www.logo.wine/a/logo/Trello/Trello-Atlassian-Logo.wine.svg" alt="" height={'auto'} width={'5%'}/>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Nav.Link href="">Workspaces</Nav.Link>
            <Nav.Link href="">Recent</Nav.Link>
            <NavDropdown title="Starred" id="navbarScrollingDropdown">
              <NavDropdown.Item href="">Some-Action</NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="">
              Recent
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Header;