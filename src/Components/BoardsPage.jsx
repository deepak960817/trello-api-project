import React, { Component } from 'react';
import Tabs from './BoardsPageTabs'
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Link} from 'react-router-dom';
import * as TrelloAPIData from './TrelloAPIData.js';
import './BoardsPage.css'

class BoardsPage extends Component {
    state = { 
        newBoardName: "",
        boardNameText: "",
        BoardsData : [],
        PageMount: true
    }

    componentDidMount(){
        TrelloAPIData.getBoards().then(result => this.setState({BoardsData : result, PageMount : false}));
    }

    handleSubmitToCreateBoard = (e) => {
        e.preventDefault();
        this.setState({newBoardName: this.state.boardNameText, boardNameText: ""}, () => this.createBoard());
    }

    createBoard = () => {TrelloAPIData.addBoards(this.state.newBoardName).then(result => this.setState({BoardsData: [...this.state.BoardsData, result]}))}


    handleDelete = (id) => {TrelloAPIData.deleteBoards(id)

        let newData = this.state.BoardsData.filter((val) => {
            return val.id !== id
        })

        this.setState({BoardsData: newData})
    }

    handleClick = (val) => {window.location.href=(`${val}`)}

    
    render() { 
        return ( this.state.PageMount ? <div className='loadingImg' style={{display:'flex', justifyContent:'center'}}><img width={'30%'} height={'30%'} src="http://cdn.onlinewebfonts.com/svg/img_462420.png" alt=""/></div> :
            <div className='bordersPage'>
                <section className='introSection'>
                    <div className='intoSectionLogo'>
                        <img src="https://upload.wikimedia.org/wikipedia/commons/6/6e/D_Magazine_logo.svg" height={'25%'} width={'25%'} alt="" />
                        <h4 className='introSectionHeading'>Deepak Trello Workspace</h4>
                    </div>
                    <Tabs  val={"Home"}/>
                </section>
                <section className='boardsDisplay' style={{display:'flex', justifyContent:'center', flexWrap:'wrap'}}>
                <Card style={{ width: '25%', maxHeight:'30vh', marginLeft: '0.5rem', marginRight: '0.5rem' }} onClick={this.handleShow}>
                <Card.Img variant="top" src="https://picsum.photos/200/100" />
                <Card.Body style={{display:'flex', justifyContent:'center', alignItems:'center', flexDirection:'column'}}>
                    <Card.Title>Create New Board</Card.Title>
                    <Form style={{display:'flex', justifyContent:'center', alignItems:'center'}}>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Control type="text" value={this.state.boardNameText} placeholder="Enter Board Name" onChange={(e) => {this.setState({boardNameText : e.target.value})}}/>
                        </Form.Group>
                        <Button style={{marginLeft: '1rem'}} variant="primary" type="submit" onClick={(e) => {this.handleSubmitToCreateBoard(e)} }>
                            Submit
                        </Button>
                    </Form>
                </Card.Body>
                </Card>

                {
                this.state.BoardsData.map((val) => {
                return(
                    <React.Fragment key={val.id}>
                        <Card style={{ width: '25%', marginLeft: '0.5rem', marginRight: '0.5rem'}}>
                        <Card.Img variant="top" src="https://placeimg.com/200/100/tech" />
                        <Card.Body style={{display:'flex', alignItems:'center', flexDirection: 'column'}}>
                            <Card.Title>{val.name}</Card.Title>
                            <div style={{display: 'flex', justifyContent: 'space-around', alignItems:'center', marginTop:'auto', marginBottom:'auto'}}>
                                <Link to={`${val.id}-${val.name}`} onClick={() => this.handleClick(`/${val.id}-${val.name}`)}>
                                <Button variant="primary" type="submit" style={{height:'100%', marginRight: '0.5rem'}}>View Board</Button>
                                </Link>
                                <Button variant="danger" type="submit" style={{height:'100%', marginRight: '0.5rem'}} onClick={() => {this.handleDelete(val.id)}}>Delete Board</Button>
                            </div>
                        </Card.Body>
                        </Card>
                    </React.Fragment>
                )})
                }


                </section>    
            </div>
        );
    }
}
 
export default BoardsPage;