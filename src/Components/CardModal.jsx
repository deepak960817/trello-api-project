import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import ProgressBar from 'react-bootstrap/ProgressBar';
import * as TrelloAPIData from './TrelloAPIData.js'

let count=0;
class CardModal extends Component {
    state = { 
        itemName: "",
        checklistItemName: "",
        checklistsData: [],
        cardID: ""
    }

    componentDidMount(){
        TrelloAPIData.getChecklists(this.props.id).then((result) => this.setState({checklistsData : result}));
    }

    handleAdd = (id) => {
        this.setState({checklistItemName: this.state.itemName, itemName: ""}, () => this.addChecklist(id));
    }

    addChecklist(id){
        TrelloAPIData.addChecklist(id, this.state.checklistItemName).then((result) => this.setState({checklistsData: [...this.state.checklistsData, result]}))
    }

    handleDelete(id){
        TrelloAPIData.deleteChecklist(id);

        let newData = this.state.checklistsData.filter((val) => {
            return val.id !== id
        })

        this.setState({checklistsData: newData})
    }

    render() { 
        
        return (
            <section>
            <div style={{display:'flex', justifyContent:'space-between', marginBottom:'1rem', alignItems:'center'}}>
                <h5>CheckList</h5>
            </div>
            <ProgressBar style={{marginBottom:'1rem'}} now={0} label={`0%`} />
            
            {
            this.state.checklistsData.map((val) => {
            return(
                <Card key={val.id} style={{width:'100%', marginTop:'1rem', backgroundColor:'#E8E8E8'}}>
                <Card.Body>
                    <Card.Title>
                        <Form.Group className="mb-3" controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label={<b>{val.name}</b>} />
                        </Form.Group>
                    </Card.Title>
                    <Button variant='danger' onClick={() => this.handleDelete(val.id)}>Delete</Button>
                </Card.Body>
                </Card>
                )})
            }

            <Form style={{marginBottom:'1rem'}}>
                <Form.Group className="mb-3" controlId="text">
                <Form.Control style={{marginTop:'1rem'}} type="email" value={this.state.itemName} placeholder="Add an Item" onChange={(e) => {this.setState({itemName: e.target.value})}}/>
                </Form.Group>
                <div style={{display:'flex', justifyContent:'flex-start'}}>
                    <Button style={{marginTop:'1rem'}} onClick={() => {this.handleAdd(this.props.id)}}>Add</Button>
                </div>
            </Form>
            </section>
        );
    }
}
 
export default CardModal;