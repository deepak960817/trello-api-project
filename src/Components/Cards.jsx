import React, { Component } from 'react';
import CardModal from './CardModal'
import Modal from 'react-bootstrap/Modal';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import * as TrelloAPIData from './TrelloAPIData.js'

class Cards extends Component {
    state = { 
        cardNameText: "",
        newCardName: "",
        cardsData: [],
        show: false,
        setShow: false,
        checklistShow: false,
        ModalContent: []
    }

    handleClose = () => this.setState({show: false, setShow: false});
    handleShow = (val) => this.setState({show: true, setShow: true}, () => this.changeContent(val));

    componentDidMount(){
        TrelloAPIData.getCards(this.props.id).then(result => this.setState({cardsData: result}))
        // if(this.props.id !== ""){
        // this.props.addnewcard().then(result => this.setState({cardsData: [...this.state.cardsData, result]}))
    }

    // handleSubmitToCreateCard = (e) => {e.preventDefault(); this.addNewCard(this.props.id, this.props.newCardName)};
    
    // this.props.addNewCard(id, newCardName).then((result) => this.setState({cardsData: [...this.state.cardsData, result]}))

    changeContent(cardModal){
        this.setState({ModalContent: cardModal})
    }

    handleDeleteCards = (id) => {TrelloAPIData.deleteCards(id)
        
        let newData = this.state.cardsData.filter((val) => {
            return val.id !== id
        })

        this.setState({cardsData: newData})
    };

    handleClickToShowModal = (e) => {
        e.preventDefault();
        this.setState({checklistShow: true})
    }
    
    render() { 
        return (
                this.state.cardsData.map((val) => {
                return (
                <div key={val.id}>
                <Card style={{marginTop: '1rem'}}>
                <Card.Body style={{display:'flex', justifyContent:'space-around', flexDirection: 'column', alignItems: 'center'}}>
                    <Card.Title>{val.name}</Card.Title>
                    <div style={{display: 'flex'}}>
                        <Button variant='primary' style={{height:'100%', marginRight: '0.5rem'}} onClick={() => {this.handleShow(val)}}>Update</Button>
                        <Button variant='danger' style={{height:'100%', marginLeft: '0.5rem'}} onClick={() => this.handleDeleteCards(val.id)}>Delete</Button>
                    </div> 
                </Card.Body>
    
                <Modal show={this.state.show} onHide={() => {this.setState({checklistShow: false, show: false})}} size="lg" aria-labelledby="example-modal-sizes-title-lg">
                    <Modal.Header closeButton>
                    <Modal.Title>{this.state.ModalContent.name}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{display:'flex'}}>
                        <section style={{width:'70%'}}>
                            {this.state.checklistShow === true ? <CardModal id={this.state.ModalContent.id}/>: ""}
                        </section>
                        <div style={{display:'flex', width:'30%', justifyContent:'center'}}>
                            <Button onClick={(e) => this.handleClickToShowModal(e)}>CheckList</Button>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose}>
                        Close
                    </Button>
                    </Modal.Footer>
                </Modal>
                </Card>
                </div>
            )})
        );
    }
}
 
export default Cards;